// CONTEXT
const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

// KEYPRESS console.log(event.keycode)
document.addEventListener("keydown", keyDown, false);
document.addEventListener("keyup", keyUp, false);
const sound = new Audio("sound.wav");
const btnReset = document.getElementById('btn-reset');
const time = document.getElementById('clock');
let playerImg = new Image();
playerImg.src = "spritesheet.png";
let left = false;
let right = false;
let jumping = false;
let visible = true;

const gravity = { x: 0, y: 0.25 }

var counter = 0;
animationInterval = setInterval(function() { 
counter++;
}, 50); 

function resetAnimation()
{
	counter = 0;
}

const player = {
    speed: 5,
    grounded: false,
    size: { x: 80, y: 123 },
    velocity: { x: 0, y: 0 },
    position: { x: 0, y: 0 },

    spritesheet: { sx: 0, sy: 0, sw: 0, sh: 0},
    state: "idle",
    frame: 0,
    clock: new Date().getMilliseconds(),

    applyGravity() {
        this.velocity.x += gravity.x;
        this.velocity.y += gravity.y;
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    },

    update() {
        if (this.position.y >= canvas.height - this.size.y && !jumping) {
            this.position.y = canvas.height - this.size.y;
            if (!jumping)
            {
                this.grounded = true;
                this.velocity.y = 0;
            }
        } else {
            if (this.velocity.y > 0) jumping = false;
            this.applyGravity();
        }
    },

    animate() {
        var anim = 0;
        if (counter > 1) 
        {
            resetAnimation();
            anim = 1;
        }
        if (anim == 1)
        {
            if (this.state == "idle")
            {
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 75;
                this.spritesheet.sy = 10;
                this.spritesheet.sw = 43;
                this.spritesheet.sh = 74;
            }
            else if (this.state == "left")
            {   
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 10 + (70 * this.frame);
                this.spritesheet.sy = 10 + 75;
                this.spritesheet.sw = 43;
                this.spritesheet.sh = 74;
            }
            else if (this.state == "right")
            {
                if (this.frame >= 3) this.frame = -1; 
                this.frame += 1;
                this.spritesheet.sx = 10 + (70 * this.frame);
                this.spritesheet.sy = 10 + 75 + 75;
                this.spritesheet.sw = 43;
                this.spritesheet.sh = 74;
            }
            anim = 0;
        }
        
    }
}

player.position.x = canvas.width / 2;
player.position.y = canvas.height - player.size.y;

btnReset.addEventListener('click', (e) => {
    player.position.x = 0;
    player.position.y = 0;
})

function update() {
    if (left) {
        player.position.x -= player.speed;
    }
    if (right) {
        player.position.x += player.speed;
    }

    player.update();
    if (!left && !right) player.state = "idle";
    if (left) player.state = "left";
    if (right) player.state = "right";
    player.animate();

    var dt = new Date();
    var day = dt.getDay();
    if (day == 1) day = "Monday";
    if (day == 2) day = "Tuesday";
    if (day == 3) day = "Wednesday";
    if (day == 4) day = "Thursday";
    if (day == 5) day = "Friday";
    if (day == 6) day = "Saturday";
    if (day == 7) day = "Sunday";
    time.innerHTML = dt.toLocaleDateString() + " - " + day + " - " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + "         animation: " + player.clock;
}

function draw() {
    ctx.setTransform(1, 0, 0, 1, -player.position.x + (canvas.width / 2), 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height); //CLEAR SCREEN
    drawImg("bg.png", new Vector2(-4091,0), new Vector2(8182, 1000));
    
     if (visible) ctx.drawImage(playerImg, player.spritesheet.sx, player.spritesheet.sy, player.spritesheet.sw, player.spritesheet.sh, player.position.x, player.position.y, player.size.x, player.size.y);

}

function keyDown(e) { // GET KEY DOWN
    if (e.key == "Right" || e.key == "ArrowRight") {
        right = true;
    }
    if (e.key == "Left" || e.key == "ArrowLeft") {
        left = true;
    }
    if (e.key == "Up" || e.key == "ArrowUp" && player.grounded && !jumping) {
        jumping = true;
        player.grounded = false;
        player.velocity.y -= 10;
    }
    if (e.key == " ")
    {
        sound.play();
    }
    if (e.key == "v")
    {
        visible = !visible;
    }
    if (e.key == "r")
    {
        window.open("https://rickastley.co.uk/"); 
    }
}

function keyUp(e) { // GET KEY UP
    if (e.key == "Right" || e.key == "ArrowRight") {
        right = false;
    }
    if (e.key == "Left" || e.key == "ArrowLeft") {
        left = false;
    }
}

function drawQuad(p1 = new Vector2(),
p2 = new Vector2(),
p3 = new Vector2(),
p4 = new Vector2(),
colour = "black",
fill = false) {
    ctx.beginPath();
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.moveTo(p1.x,p1.y);
    ctx.lineTo(p2.x,p2.y);
    ctx.lineTo(p3.x,p3.y);
    ctx.lineTo(p4.x,p4.y);
    ctx.lineTo(p1.x,p1.y);
    if (fill) ctx.fill();
    ctx.stroke();
}

function drawText(text = "Hello World", font = "10px monospace", colour = "Black", position = new Vector2()) {
    ctx.font = font;
    ctx.fillStyle = colour;
    ctx.fillText(text, position.x, position.y);
}

function drawImg(src, position = new Vector2(), size = new Vector2()) {
    let img = new Image();
    img.src = src;
    ctx.drawImage(img, position.x, position.y, size.x, size.y);
}

function drawCircle(position = new Vector2(), radius = 1, colour = "Black", fill = false) {
    ctx.beginPath();
    ctx.strokeStyle = colour;
    ctx.fillStyle = colour;
    ctx.arc(position.x, position.y, radius, 0, 2 * Math.PI);
    if (fill) ctx.fill();
    ctx.stroke();
}

function Vector2(x = 0,y = 0) {
    this.x = x;
    this.y = y;
}

function GameLoop() {
    update();
    draw()
    window.requestAnimationFrame(GameLoop);
}

window.requestAnimationFrame(GameLoop);
